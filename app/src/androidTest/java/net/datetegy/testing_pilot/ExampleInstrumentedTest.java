package net.datetegy.testing_pilot;

import android.content.Context;

import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static android.app.PendingIntent.getActivity;
import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.action.ViewActions.actionWithAssertions;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.hamcrest.text.MatchesPattern;
//;import javax.annotation.MatchesPattern;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("net.datetegy.testing_pilot", appContext.getPackageName());
    }
    /*public void testtextfield(){
        onView(withId(R.id.num)).perform(typeText("Vakchija"));
        onView(withId(R.id.num)).perform(typeText("123lk"));
        onView(withId(R.id.alpha_num)).perform(clearText());
        onView(withId(R.id.alpha_num)).perform(typeTextIntoFocusedView("VAKCHIJA"));

    }*/
    /*
    private static Computation computation = null;
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    @Test
    public void file_isCreated() throws IOException {
        folder.newFolder("MyTestFolder");
        File testFile = folder.newFile("MyTestFile.txt");
        assertTrue(testFile.exists());
    }
    @BeforeClass
    public static void CreateComputationObject() {
        computation = new Computation();
    }
    @AfterClass
    public static void DestroyComputationObject() {
        computation = null;
    }
    @Test
    public void sum_isCorrect() {
        assertEquals(4, computation.Sum(2,2));
    }
    @Test
    public void multiply_isCorrect() {
        assertEquals(4, computation.Multiply(2,2));
    }
    @Test
    public void list_hasValue() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("John");
        assertThat("Is list has John?", list, hasItem("John"));
    }*/
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);
    @Test
    public void view_isCorrect() {
        //onView(withText("hello")).check(matches(isDisplayed()));
    }
    /*@Test
    public void a_test_type_of_matchers() {
        //onView(withId(R.id.alpha_num));
        //onView(allOf(withId(R.id.alpha_num), hasContentDescription()));

        //il existe un Textview de id pilot having testing_pilot as test value
        onView(withId(R.id.pilot)).check(matches(withText("testing_pilot")));
        // pas de Textview avec testing
        onView(withText("testing")).check(doesNotExist());

        //onView(withId(R.id.button)).perform(typeText("Hello World!"));  // 'Animations or transitions are enabled on the target device.
        // onView(withId(R.id.button)).perform(clearText());
        onView(withId(R.id.alpha_num)).perform(typeText("vakchija"));
        onView(withId(R.id.num)).perform(typeText("123"));
        onView(withId(R.id.num)).perform(typeText("123vaki"));

        // performing button
        onView(withId(R.id.num)).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.num)).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.buttonTest)).perform(click()).check(matches(isDisplayed()));

    }*/
    @Test
    public void a_fill_first_field_with_text(){
        onView(withId(R.id.alpha_num)).perform(typeText("vakchija"),ViewActions.closeSoftKeyboard());
        //onView(withId(R.id.alpha_num)).perform(replaceText("hello tout le monde"),ViewActions.closeSoftKeyboard());
    }
    @Test
    public void b_fill_second_field_with_number(){
        onView(withId(R.id.num)).perform(typeText("123"),ViewActions.closeSoftKeyboard());
    }
    @Test
    public void c_fill_second_field_with_numbertext(){
        onView(withId(R.id.num)).perform(typeText("123vaki"),ViewActions.closeSoftKeyboard());
    }
    @Test
    public void d_check_first_field_is_empty(){
        //ViewInteraction editText = onView(withId(R.id.num));
        onView(withId(R.id.alpha_num)).perform(typeText("vakchija"),ViewActions.closeSoftKeyboard());
        EditText editedit1 = mActivityTestRule.getActivity().findViewById(R.id.alpha_num);
        String message1 = editedit1.getText().toString();
        String empty = "";
        assertNotEquals(message1,empty);
    }
    @Test
    public void e_check_second_field_is_empty(){
        onView(withId(R.id.num)).perform(typeText("123vaki"),ViewActions.closeSoftKeyboard());
        EditText editedit2 = mActivityTestRule.getActivity().findViewById(R.id.num);
        String message2 = editedit2.getText().toString();
        String empty = "";
        assertNotEquals(message2,empty);
    }
    @Test
    public void f_fill_first_and_seccond_empty(){
        onView(withId(R.id.alpha_num)).perform(typeText(""),ViewActions.closeSoftKeyboard());
        onView(withId(R.id.num)).perform(typeText(""),ViewActions.closeSoftKeyboard());
    }
    @Test
    public void g_check_both_empty(){
        onView(withId(R.id.alpha_num)).perform(typeText(""),ViewActions.closeSoftKeyboard());
        EditText editedit3 = mActivityTestRule.getActivity().findViewById(R.id.alpha_num);
        String message3 = editedit3.getText().toString();
        String empty = "";
        assertNotEquals(message3,empty);
        onView(withId(R.id.num)).perform(typeText(""),ViewActions.closeSoftKeyboard());
        EditText editedit4 = mActivityTestRule.getActivity().findViewById(R.id.num);
        String message4 = editedit4.getText().toString();
        assertNotEquals(message4,empty);
    }
    @Test
    public void h_check_both_null(){
        EditText editedit5 = mActivityTestRule.getActivity().findViewById(R.id.alpha_num);
        String message5 = editedit5.getText().toString();
        String val_null = null;
        assertNotNull(message5,val_null);
        EditText editedit6 = mActivityTestRule.getActivity().findViewById(R.id.num);
        String message6 = editedit6.getText().toString();
        assertNotEquals(message6,val_null);
    }
    @Test
    public void i_check_second_is_number(){
        onView(withId(R.id.num)).perform(typeText("123"),ViewActions.closeSoftKeyboard());
        //onView(withId(R.id.num)).check(matches("[0-9]+"));
        EditText editedit7 = mActivityTestRule.getActivity().findViewById(R.id.num);
        String message7 = editedit7.getText().toString();
        //String val_num = "[a-zA-Z]+" ;
        String val_num = "[0-9]+" ;
        //message7.contains(val_num);
        //message7.matches(val_num);
        assertEquals(message7,val_num );
        //actionWithAssertions()
        //onView(withId(R.id.num)).check(matches((withText("[a-zA-Z]+"))));*/
    }
    @Test
    public void performing_button(){
        onView(withId(R.id.buttonTest)).perform(click()).check(matches(isDisplayed()));
    }

    @Test
    public void test_field_vide(){
        onView(withId(R.id.alpha_num)).perform(typeText("vakchija"));
        onView(withId(R.id.num)).perform(typeText(""));
        // checking that a field is not
        onView(withId(R.id.num)).check(matches((withText(""))));
        //isNotChecked(onView(withId(R.id.num)));
        //onView(withId(R.id.num)).check(matches(isEditTextValueEqualTo(R.id.num,"")));
        //onView(withId(viewId)).check(matches(isEditTextValueEqualTo(viewId, value)));
    }
    @Test
    public void test_field_not_vide(){
        onView(withId(R.id.alpha_num)).perform(typeText(""));
        onView(withId(R.id.num)).perform(typeText("_330755788057"));

        onView(withId(R.id.num)).check(matches(withText(MatchesPattern.matchesPattern("^(\\+\\d{1,3}[- ]?)?\\d{10}$"))));
        //onView(withId(R.id.num)).check(matches(withText("330755788057")));
    }

    /*
    *
    *   ^ start of line
        A + followed by \d+ followed by a or - which are optional.
        Whole point two is optional.
        Negative lookahead to make sure 0s do not follow.
        Match \d+ 10 times.
        Line end.
    *
    * */
    @Test
    public void test_pattern_that_supports_mobile_number(){
        onView(withId(R.id.alpha_num)).perform(typeText(""));
        //wrong mobile number format
        onView(withId(R.id.num)).perform(typeText("_330755788057"));
        //correct mobile number format
        onView(withId(R.id.num)).perform(typeText("+330755788057"));

        onView(withId(R.id.num)).check(matches(withText(MatchesPattern.matchesPattern("^(\\+\\d{1,3}[- ]?)?\\d{10}$"))));
        //onView(withId(R.id.num)).check(matches(withText("330755788057")));
    }


    @Test
    public void spinnerTest(){

        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("item5"))).perform(click());
        onView(withId(R.id.spinnerTest)).check(matches(withSpinnerText(containsString("item5"))));
    }

    @Test
    public void a_optiontest_1_selectSecondOption(){
        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("hide_first_text_field"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(not(isDisplayed())));
    }

    @Test
    public void a_optiontest_2_selectThirdOption(){
        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("hide_first_second_text_field"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(not(isDisplayed())));
        onView(withId(R.id.num)).check(matches(not(isDisplayed())));
    }

    @Test
    public void ta_optiontest_3_selectFourthOption(){
        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("hide_second_text_field"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(isDisplayed()));
        onView(withId(R.id.num)).check(matches(not(isDisplayed())));
    }

    @Test
    public void a_optiontest_4_selectSixthOption(){
        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("visible_all"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(isDisplayed()));
        onView(withId(R.id.num)).check(matches(isDisplayed()));
    }

    @Test
    public void a_optiontest_5_selectSequenceAllOptions(){
        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("hide_first_text_field"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(not(isDisplayed())));

        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("hide_first_second_text_field"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(not(isDisplayed())));
        onView(withId(R.id.num)).check(matches(not(isDisplayed())));

        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("hide_second_text_field"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(isDisplayed()));
        onView(withId(R.id.num)).check(matches(not(isDisplayed())));

        onView(withId(R.id.spinnerTest)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("visible_all"))).perform(click());
        onView(withId(R.id.alpha_num)).check(matches(isDisplayed()));
        onView(withId(R.id.num)).check(matches(isDisplayed()));


    }

}

