package net.datetegy.testing_pilot;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // you need to have a list of data that you want the spinner to display
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("item1");
        spinnerArray.add("hide_first_text_field");
        spinnerArray.add("hide_first_second_text_field");
        spinnerArray.add("hide_second_text_field");
        spinnerArray.add("item5");
        spinnerArray.add("visible_all");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.spinnerTest);
        sItems.setAdapter(adapter);


        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==1)
                {
                    findViewById(R.id.alpha_num).setVisibility(View.INVISIBLE);
                }
                else if(i==2)
                {
                    findViewById(R.id.alpha_num).setVisibility(View.INVISIBLE);
                    findViewById(R.id.num).setVisibility(View.INVISIBLE);
                }
                else if(i==3)
                {
                    findViewById(R.id.num).setVisibility(View.INVISIBLE);
                }
                else if(i==5)
                {
                    findViewById(R.id.alpha_num).setVisibility(View.VISIBLE);
                    findViewById(R.id.num).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
