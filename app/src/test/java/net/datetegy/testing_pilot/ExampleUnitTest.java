package net.datetegy.testing_pilot;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @AfterClass
    public static void DestroyComputationObject() {
        net.datetegy.testing_pilot.Computation computation = null;
    }
    @Test
    public void sum_isCorrect() {
        net.datetegy.testing_pilot.Computation computation = new Computation();
        assertEquals(4, computation.Sum(2,2));
    }
    @Test
    public void multiply_isCorrect() {
        net.datetegy.testing_pilot.Computation computation = new Computation();
        assertEquals(4, computation.Multiply(2,2));
    }
    //net.datetegy.testing_pilot.MainActivity activity = new MainActivity();
    //assertEquals(true, activity.test_field_vide());
}